"""
Simple Car class with properties registration number and colour.
"""
class Car:
    def __eq__(self, car):
        if car is None:
            return False
        return self.registration_number == car.registration_number and \
               self.colour == car.colour

# prevent duplicate keys in a dict
# just to further reinforce that we are hashing based on the reg number and colour
    def __hash__(self):
        return hash((self.registration_number, self.colour))
        

# >>> car0 = Car(112, 'white')
# >>> car1 = Car(112, 'white')
# >>> car2 = Car(123, 'brown')
# >>> car0 == car1
# True

    def __init__(self, registration_number, colour):
        self.registration_number = registration_number
        self.colour = colour
