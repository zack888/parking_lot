"""
Static variables for command names.
"""

class Command:
    CREATE_PARKING_LOT = 'create_parking_lot'
    PARK = 'park'
    LEAVE = 'leave'
    STATUS = 'status'
    REGISTRATION = 'registration_numbers_for_cars_with_colour'
    SLOTS_WITH_COLOUR = 'slot_numbers_for_cars_with_colour'
    SLOT_FOR_REGISTRATION = 'slot_number_for_registration_number'
