"""
Contains
- utility static variables and static methods.
- ParkingLot class.
"""
from string import Template
import heapq
from ParkingSystem import ParkingSystem
from Car import Car

CREATE_PARKING_LOT = 'create_parking_lot'
PARK = 'park'
LEAVE = 'leave'
STATUS = 'status'
REGISTRATION = 'registration_numbers_for_cars_with_colour'
SLOTS_WITH_COLOUR = 'slot_numbers_for_cars_with_colour'
SLOT_FOR_REGISTRATION = 'slot_number_for_registration_number'
EXIT = 'exit'
VALID_NAMES = [CREATE_PARKING_LOT, PARK, LEAVE, STATUS, REGISTRATION,
               SLOTS_WITH_COLOUR, SLOT_FOR_REGISTRATION, EXIT]

# can be implemented as a static method in the ParkingLot Class with staticmethod decorator
def process(command, parking_system):
    """
    Idempotent function for performing a command on a parking lot object.
    :param command: Command to perform on parking lot object.
    :param parking_lot: Parking lot object.
    :return: Parking lot object after performing command.
    """
    check_arguments(command)
    command = command.split(' ')
    if parking_system.length() == 0 and command[0] != CREATE_PARKING_LOT:
        print('Parking lot not created yet.')
        return
    # if len(parking_lots) and command[0] == CREATE_PARKING_LOT:
    #     print('Only 1 parking lot is allowed.')
    name = command[0]
    num_parking_lots = command[2]
    if name == CREATE_PARKING_LOT:
        # parking_lot = ParkingLot(command[1])
        parking_lots = [ParkingLot(command[1]) for i in range(int(num_parking_lots))]
        parking_system = ParkingSystem(parking_lots)
    else:
        parking_system.process_command(command)

    return parking_system
    

def command_name_valid(name):
    """
    Check if input command name is valid.
    :param name: Input command name.
    :return: void. Throws error if name is invalid.
    """
    if name not in VALID_NAMES:
        print('{} is not a valid command.\nValid commands are {}'\
              .format(name, VALID_NAMES))

def check_arguments(command, line_num=None):
    """
    Ensure there are correct number and types of arguments
    :param command: Command name and arguments e.g leave 4.
    :param line_num: Line number of command.
    :return: void. Prints error message if appropriate.
    """
    command = command.split(' ')
    name = command[0]
    if name in [SLOTS_WITH_COLOUR, SLOT_FOR_REGISTRATION, CREATE_PARKING_LOT, LEAVE]:
        assert_len(command, line_num, 2)
        if name == CREATE_PARKING_LOT or name == LEAVE:
            if not command[1].isdigit():
                print('Parameter of {} command should be {}.'.format(name, 'int'))

    # todo: should exit if command is not valid
    if name == PARK and assert_len(command, line_num, 3):
        registration_number = command[1]
        assert_max_255(registration_number)

        colour = command[2]
        assert_type(name, colour, line_num, str)
        assert_max_255(colour)


def assert_len(command, line_num, valid_length):
    """
    Validate command for correct number of parameters.
    :param command: List of strings from split command.
    :param line_num: Line number if read from file.
    :param valid_length: Valid number of parameters.
    :return: boolean.
    """
    if len(command) != valid_length:
        assert_line_helper(line_num)
        print('Should have {} parameters on {} command'.format(valid_length, command[0]))
        return False
    return True

def assert_type(name, parameter, line_num, valid_type):
    """
    Validate command paramters are correct data type.
    :param name: Command name.
    :param parameter: Command parameter.
    :param line_num: Line number if read from file.
    :param valid_type: Data type to be validated against.
    :return: void.
    """
    if not isinstance(parameter, valid_type):
        assert_line_helper(line_num)
        print('Parameter of {} command should be {}'.format(name, valid_type))

def assert_line_helper(line_num):
    """
    Helper method to print line number if any.
    :param line_num: Line number if any.
    :return: void.
    """
    if line_num is not None:
        print('Line {}:'.format(line_num), end='')

def assert_max_255(prop, line_num=None):
    """
    Validation method for car property unlikely to be greater than 255 chars.
    :param prop: Car property to be validated.
    :param line_num: Line number if read from file.
    :return: void.
    """
    if len(prop) > 255:
        assert_line_helper(line_num)
        print('{} is longer than 255 characters. This does not make sense.'
              .format(prop))


class ParkingLot:
    def process_command(self, command):
        """
        Process a command.
        :param command: List read from a line in file or from console.
        :return: void.
        """
        name = command[0]
        if name == PARK:
            print(self.park(command[1], command[2]))
        if name == LEAVE:
            print(self.leave(command[1]))
        if name == STATUS:
            print(self.status())
        if name == REGISTRATION:
            print(self.registration_numbers_for_cars_with_colour(command[1]))
        if name == SLOTS_WITH_COLOUR:
            print(self.slot_numbers_for_cars_with_colour(command[1]))
        if name == SLOT_FOR_REGISTRATION:
            print(self.slot_number_for_registration_number(command[1]))

    def park(self, registration_number, colour):
        """
        Park car in free lot with smallest number.
        :param registration_number: Car registration number.
        :param colour: Car colour.
        :return: Message string.
        """
        if len(self.free_lot_numbers) == 0:
            return 'Sorry, parking lot is full'

        if registration_number in self.car_registration_numbers:
            for car in self.cars:
                if car.registration_number == registration_number:
                    return ('Error. Registration numbers should be unique. Lot {}'
                          ' has car with colour {} and same registration number'
                          .format(self.cars.index(car), car.colour))
        self.car_registration_numbers.add(registration_number)

        # Park in first available lot.
        # [2,1,0]
        # [2,1]
        # [2]
        # [2,0]
        # [2,0,1]
        # park -> error

        # free_lot_number = self.free_lot_numbers.pop()
        free_lot_number = heapq.heappop(self.free_lot_numbers)
        # Mark lot as occupied.
        self.cars[free_lot_number] = Car(registration_number, colour)

        if len(self.max_occupied_slot_number) == 0 or \
                free_lot_number > self.max_occupied_slot_number[-1]:
            self.max_occupied_slot_number.append(free_lot_number)
        if len(registration_number) >= self.registration_number_max_length[-1]:
            self.registration_number_max_length.append(len(registration_number))
        if len(colour) >= self.colour_max_length[-1]:
            self.colour_max_length.append(len(colour))

        return 'Allocated slot number: {}'.format(free_lot_number + 1)

    def leave(self, slot_number):
        """
        Remove a car from this parking lot object.
        :param slot_number: Slot number of car to remove.
        :return: Message string.
        """
        actual_slot_number = int(slot_number) - 1
        if actual_slot_number < 0 or actual_slot_number in self.free_lot_numbers:
            return 'Invalid slot number.'

        if actual_slot_number == self.max_occupied_slot_number[-1]:
            self.max_occupied_slot_number.pop()
        car = self.cars[actual_slot_number]
        if len(car.registration_number) == self.registration_number_max_length:
            self.registration_number_max_length.pop()
        if len(car.colour) == self.colour_max_length:
            self.colour_max_length.pop()

        self.car_registration_numbers.remove(car.registration_number)

        # Mark lot as free.
        # todo: by appending to free_lot_numbers, might be a bug here
        # self.free_lot_numbers.append(actual_slot_number)
        heapq.heappush(self.free_lot_numbers,actual_slot_number)
        heapq.heapify(self.free_lot_numbers)
        self.cars[actual_slot_number] = None

        return 'Slot number {} is free'.format(slot_number)

    def status(self):
        """
        Print status of parking lot.
        :return: Message string.
        """
        # Length of registration number.
        template = Template("{0:$slot}{1:$regis}{2:$colour}")
        template = template.substitute(
            slot=max(len('Slot No.'), len(str(len(self.cars)))) + 4,
            regis=max(
                len('Registration No'),
                self.registration_number_max_length[-1] + 4
            ) + 2,
            colour=max(
                len('Colour'),
                self.colour_max_length[-1]
            )
        )
        status_string = template.format('Slot No.', 'Registration No', 'Colour')
        for car in self.cars:
            if car is not None:
                status_string += '\n' + template.format(str(self.cars.index(car) + 1),
                                      car.registration_number.upper(),
                                      car.colour.capitalize()).strip()
        return status_string

    def registration_numbers_for_cars_with_colour(self, colour):
        """
        O(n) time complexity.
        Can be optimized to O(1) average by maintaining a dict of colour to list
        of registration numbers, with no increase in space complexity.
        :param colour: Car colour.
        :return: Message string.
        """
        registration_numbers = [car.registration_number.upper() for car in
                                self.cars
                                if car is not None and car.colour == colour]
        # if len(registration_numbers) > 0:
            # return ', '.join(registration_numbers)
        return registration_numbers
        # return 'Not found'

    def slot_numbers_for_cars_with_colour(self, colour):
        """
        Same optimization possible as with
        print_registration_numbers_for_cars_with_colour method.
        :param colour: Car colour.
        :return: Message string.
        """
        slot_numbers = [str(i + 1) for i, car in enumerate(self.cars)
                        if car is not None and car.colour == colour]
        if len(slot_numbers) > 0:
            return ', '.join(slot_numbers)
        return 'Not found'

    def slot_number_for_registration_number(self, registration_number):
        """
        Same optimization possible as with
        print_registration_numbers_for_cars_with_colour method.
        :param registration_number: Car registration number.
        :return: Message string.
        """
        for i, car in enumerate(self.cars):
            if car is not None and car.registration_number == registration_number:
                return str(i + 1)
        return 'Not found'


    def __init__(self, slots):
        """
        Create a new parking lot with specified number of slots.
        :param slots: Number of slots this parking
        """

        # todo: implement self.free_lot_numbers using a min-heap for constant lookup Ologn addition and removal of elements
        # Store free lot numbers in a stack for O(1) push and pop.
        self.free_lot_numbers = [i for i in reversed(range(int(slots)))]
        heapq.heapify(self.free_lot_numbers)

        self.cars = [None]*int(slots)

        # Set to ensure car registration numbers are unique.
        self.car_registration_numbers = set()

        # implemented as a stack just in case we find a longer word
        # For pretty printing of parking lot status.
        self.max_occupied_slot_number = []
        self.registration_number_max_length = [0]
        # length of the longest word
        self.colour_max_length = [0]

        print('Created a parking lot with {} slots'.format(slots))
