from Command import Command

class ParkingSystem:
    def __init__(self,arr):
        self.parkingObjs = arr
    def length(self):
        return len(self.parkingObjs)
    def process_command(self,command):
        if command[0] == Command.REGISTRATION:
            allthreearr = []
            for parkinglot in self.parkingObjs:
                allthreearr += parkinglot.registration_numbers_for_cars_with_colour(command[1])

            return ', '.join(allthreearr)

        elif command[0] == Command.SLOT_FOR_REGISTRATION:
            allthreearr = []
            for parkinglot in self.parkingObjs:
                allthreearr += parkinglot.slot_number_for_registration_number(command[1])

            return ', '.join(allthreearr)
        # elif command[0] == Command.PARK:
        #     min1 = self.parkingObjs[0].free_lot_numbers.