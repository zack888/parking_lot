# Parking Lot Game
For interactive mode, program does not exit on invalid input.

If using a file input, program exits if there are any invalid commands.

## Assumptions 
- Only 1 parking lot is allowed.
- A parking lot cannot be deleted.
- If using input file, only 1 input file is allowed.
- Every car occupies 1 spot in the parking lot.
- Car registration numbers unique uppercase strings.
- Registration number should be a sensible length, no longer than 255
characters.
- String to label car's colour e.g. "black", "white" should be a
sensible length, no longer than 255 characters.

## Approach
We have separate scripts for interactive input and file input, to
isolate file validation in the file input script.

In these scrips, the parking lot object is initilized with a
`create_parking_lot` command. Subsequent commands are run with the
functional `process` method, that accepts the (command, parking lot
object) as arguments and returns a new parking lot object. This is
stateless and idempotent.

## Tests
Run unit tests with following command, or from PyCharm IDE.
```
python3 TestParkingLot.py
```

## Author
Tan Zhi Long

