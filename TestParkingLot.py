from unittest.mock import MagicMock

import unittest
import io
import sys

from ParkingLot import *

# check what we expect to be printed to terminal
def redirect_print():
    """
    Redirect print method to variable
    :return: variable that receives print output.
    """
    captured_output = io.StringIO()  # Create StringIO object
    sys.stdout = captured_output  # and redirect stdout.
    return captured_output


class TestParkingLot(unittest.TestCase):
    def setUp(self):
        self.parking_lot = ParkingLot(5)

    def test_create_parking_lot(self):
        parking_lot = ParkingLot(5)
        self.assertEqual(len(parking_lot.free_lot_numbers), 5)

    def test_process_command_methods_called(self):
        # to create a mock park function
        # not actually running park, just checking if park would be called
        self.parking_lot.park = MagicMock()
        self.parking_lot.process_command(['park', 'REGIS', 'White'])
        self.parking_lot.park.assert_called_with('REGIS', 'White')

        self.parking_lot.leave = MagicMock()
        self.parking_lot.process_command(['leave', '5'])
        self.parking_lot.leave.assert_called_with('5')

        self.parking_lot.status = MagicMock()
        self.parking_lot.process_command(['status'])
        self.parking_lot.status.assert_called_with()

        self.parking_lot.registration_numbers_for_cars_with_colour = MagicMock()
        self.parking_lot.process_command([
            'registration_numbers_for_cars_with_colour', 'White'
        ])
        self.parking_lot.registration_numbers_for_cars_with_colour\
            .assert_called_with('White')

        self.parking_lot.slot_numbers_for_cars_with_colour = MagicMock()
        self.parking_lot.process_command([
            'slot_numbers_for_cars_with_colour', 'White'
        ])
        self.parking_lot.slot_numbers_for_cars_with_colour.assert_called_with('White')

        self.parking_lot.slot_number_for_registration_number = MagicMock()
        self.parking_lot.process_command([
            'slot_number_for_registration_number', 'REGIS'
        ])
        self.parking_lot.slot_number_for_registration_number.assert_called_with('REGIS')

    def test_park(self):
        self.parking_lot.park('regis-num-0', 'Red')
        self.parking_lot.park('regis-num-1', 'Blue')
        self.assertEqual(self.parking_lot.cars[0], Car('regis-num-0', 'Red'))
        self.assertEqual(self.parking_lot.cars[1], Car('regis-num-1', 'Blue'))
        self.assertTrue(all(car is None for car in self.parking_lot.cars[2:]))

    def test_unique_car_registration_numbers(self):
        self.parking_lot.park('regis-num-0', 'Red')
        self.parking_lot.park('regis-num-0', 'Green')
        self.assertEqual(self.parking_lot.cars[0], Car('regis-num-0', 'Red'))
        self.assertTrue(all(car is None for car in self.parking_lot.cars[1:]))

    def test_leave(self):
        self.parking_lot.park('regis-num-0', 'Red')
        self.parking_lot.park('regis-num-1', 'Blue')
        self.parking_lot.park('regis-num-2', 'Blue')
        self.parking_lot.leave(2)
        self.assertEqual(self.parking_lot.cars[1], None)

    def test_park_then_leave_then_park(self):
        self.parking_lot.park('regis-num-0', 'Red')
        self.parking_lot.park('regis-num-1', 'Blue')
        self.parking_lot.park('regis-num-2', 'Blue')
        self.parking_lot.leave(2)
        self.parking_lot.park('regis-num-3', 'Blue')
        self.assertEqual(self.parking_lot.cars[1], Car('regis-num-3', 'Blue'))

    def test_status(self):
        self.parking_lot.park('regis-a', 'Red')
        self.parking_lot.park('regis-b', 'Blue')
        self.parking_lot.park('regis-c', 'Blue')
        self.assertIn('REGIS-A', self.parking_lot.status())
        self.assertIn('\n2', self.parking_lot.status())
        self.parking_lot.leave(2)
        self.assertNotIn('REGIS-B', self.parking_lot.status())
        self.assertNotIn('\n2', self.parking_lot.status())

    def test_registration_numbers_for_cars_with_colour(self):
        self.parking_lot.park('regis-a', 'Red')
        self.assertEqual(
            self.parking_lot.registration_numbers_for_cars_with_colour('Red'),
            'REGIS-A'
        )
        self.parking_lot.park('regis-b', 'Red')
        self.assertEqual(
            self.parking_lot.registration_numbers_for_cars_with_colour('Red'),
            'REGIS-A, REGIS-B'
        )
        self.parking_lot.leave(1)
        self.parking_lot.leave(2)
        self.assertEqual(
            self.parking_lot.registration_numbers_for_cars_with_colour('Red'),
            'Not found'
        )

    def test_slot_numbers_for_cars_with_colour(self):
        self.parking_lot.park('regis-a', 'Red')
        self.assertEqual(
            self.parking_lot.slot_numbers_for_cars_with_colour('Blue'),
            'Not found'
        )
        self.parking_lot.park('regis-b', 'Blue')
        self.parking_lot.park('regis-c', 'Blue')
        self.assertEqual(
            self.parking_lot.slot_numbers_for_cars_with_colour('Red'), '1'
        )
        self.assertEqual(
            self.parking_lot.slot_numbers_for_cars_with_colour('Blue'), '2, 3'
        )
        self.parking_lot.leave(2)
        self.assertEqual(
            self.parking_lot.slot_numbers_for_cars_with_colour('Blue'), '3'
        )

    def test_slot_number_for_registration_number(self):
        self.parking_lot.park('regis-a', 'Red')
        self.assertEqual(
            self.parking_lot.slot_number_for_registration_number('regis-a'), '1'
        )
        self.assertEqual(
            self.parking_lot.slot_number_for_registration_number('regis-b'),
            'Not found'
        )

    def test_command_name_valid(self):
        captured_output = redirect_print()
        command_name_valid('invalid_command')
        self.assertIn('invalid_command is not a valid command', captured_output.getvalue())

        captured_output = redirect_print()
        command_name_valid('create_parking_lot')
        self.assertEqual(captured_output.getvalue(), '')

    def test_process_invalid_command(self):
        captured_output = redirect_print()
        process('create_parking_lot 1', self.parking_lot)
        self.assertEqual(captured_output.getvalue(), 'Only 1 parking lot is allowed.\n')

        captured_output = redirect_print()
        self.parking_lot = None
        process('leave 1', self.parking_lot)
        self.assertEqual(captured_output.getvalue(), 'Parking lot not created yet.\n')

    def test_check_arguments(self):
        captured_output = redirect_print()
        check_arguments('create_parking_lot 10 20')
        self.assertEqual(captured_output.getvalue(),
                         'Should have 2 parameters on create_parking_lot command\n')

        captured_output = redirect_print()
        check_arguments('create_parking_lot ten')
        self.assertEqual(captured_output.getvalue(),
                         'Parameter of create_parking_lot command should be int.\n')

        captured_output = redirect_print()
        check_arguments('leave ten')
        self.assertEqual(captured_output.getvalue(),
                         'Parameter of leave command should be int.\n')

        captured_output = redirect_print()
        check_arguments('park regis-a')
        self.assertEqual(
            'Should have 3 parameters on park command\n',
            captured_output.getvalue()
        )

        captured_output = redirect_print()
        check_arguments('slot_numbers_for_cars_with_colour White Black')
        self.assertEqual(
            'Should have 2 parameters on slot_numbers_for_cars_with_colour command\n',
            captured_output.getvalue()
        )

        captured_output = redirect_print()
        long_string = 'x'*256
        check_arguments('park {} White'.format(long_string))
        self.assertEqual(
            '{} is longer than 255 characters. This does not make sense.\n'
            .format(long_string),
            captured_output.getvalue()
        )

    # newline added after implementation of heap
    def test_park_park_park_leave_leave_park(self):
        self.parking_lot.park('regis 0', 'Red')
        self.parking_lot.park('regis 1', 'White')
        self.parking_lot.park('regis 2', 'Blue')
        self.parking_lot.leave(1)
        self.parking_lot.leave(2)
        self.parking_lot.park('regis 3', 'Yellow')
        self.assertEqual(self.parking_lot.cars[0], Car('regis 3', 'Yellow'))
        self.assertEqual(self.parking_lot.cars[2], Car('regis 2', 'Blue'))


if __name__ == '__main__':
    unittest.main()
