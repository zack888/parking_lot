"""
Script for processing an input file.
"""
import sys

from ParkingLot import check_arguments, CREATE_PARKING_LOT, process

def validate(input_file):
    # Preliminary check for argument count and type in file.
    # Strip blank lines.
    # Assumes that input file fits in memory.
    lines = [line.strip() for line in input_file.read().split('\n') if line.strip() != '']

    # create_parking_lot should be first line and should only occur once.
    if lines[0].split(' ')[0] != CREATE_PARKING_LOT:
        print('First line should be create_parking_lot.')
        exit(1)
    create_parking_lot_count = 0
    for i, command in enumerate(lines):
        if command == CREATE_PARKING_LOT:
            create_parking_lot_count += 1
        if create_parking_lot_count > 1:
            print('Error. {} should only occur once.'.format(CREATE_PARKING_LOT))
            exit(1)

        # would want to terminate if check_arguments is False, TBC
        check_arguments(command, i)


ifile = open(sys.argv[1])
# Ensure input file is valid before running Parking program on it.
validate(ifile)

parking_lot = None
for line in open(sys.argv[1]):
    # process fn is stateless, style used for idempotent fns
    parking_lot = process(line.strip().lower(), parking_lot)
