"""
Script for interactive parking lot game.
"""
from ParkingLot import CREATE_PARKING_LOT, EXIT, command_name_valid, process

parking_lot = None

command = ''
while True:
    command = input().strip().lower()
    name = command.split(' ')[0]
    command_name_valid(name)
    if name == EXIT:
        exit(0)

    # commented out these lines

    # if parking_lot is None and name != CREATE_PARKING_LOT:
    #     print('Parking lot not created yet.')
    else:
        parking_lots = process(command, parking_lot)

